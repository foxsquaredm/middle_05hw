
#include <iostream>

class Fraction
{
public:
	//Fraction(int value1, int value2) : numerator(value1), denominator(value2) 
	Fraction(int value1, int value2)
	{
		try
		{
			checkNumber(value2);
		}
		catch (const std::runtime_error& err)
		{
			std::cerr << "Runtime error: " << err.what() << '\n';
		}
	}
	//Fraction(int value1, int value2);
	~Fraction();

	int checkNumber(int valueCh)
	{
		if (valueCh == 0)
		{
			std::cout << "we have error...." << '\n';
			throw std::runtime_error("Bad things happened.");
		}

		std::cout << "we have good news...." << '\n';
	}

private:

	int numerator = 0;
	int denominator = 0;

};

Fraction::~Fraction()
{
}


float GetValueNumber(int NomNumber)
{
	float number = 0;

	std::cout << "it`s number#:" << NomNumber<< "\n";
	std::cin >> number;

	if (std::cin.fail()) {
		std::cin.clear();
		//std::cin.ignore(numeric_limits<streamsize>::max(), '\n');

		throw "�� ����� �� �����!";
	}
	
	return number;
}

int main()
{

	setlocale(LC_ALL, "ru");

	////////////////////////////////////////////
	//part 1 
	std::cout << "part 1" << "\n";
	Fraction example1(12, 34);
	Fraction example2(5, 0);
	std::cout << "\n";
	
	////////////////////////////////////////////
	//part 2	

	int NomNumber = 1;


	std::cout << "part 2" << "\n";
	float number1 = 0;
	float number2 = 0;

	std::cout << "Plaese enter 2 number:" << "\n";
	
	bool itsCancel = false;

	try
	{
		number1 = GetValueNumber(NomNumber);
	}
	catch (const char* MessErr)
	{
		std::cout << "error enter # " << NomNumber << ": "  << MessErr << '\n';
		itsCancel = true;
	}

	if (!itsCancel)
	{
		NomNumber++;
		try
		{
			number2 = GetValueNumber(NomNumber);
		}
		catch (const char* MessErr)
		{
			std::cout << "error enter # " << NomNumber << ": " << MessErr << '\n';
			itsCancel = true;
		}
	}

	if (!itsCancel)
	{
		float resSumm = number1 + number2;
		std::cout << "����� = " << resSumm << std::endl;

	}
	
	return 0;
}